# Bitcoin Auditor

Simple Vue.js app for viewing bitcoin miner stats from [slushpool.com](https://slushpool.com/).

[Demo Site](http://bitcoin.bhdouglass.com/#demo)

## Development

* Install npm dependencies:
    * Run: `npm install`
* Start the app:
    * Run: `npm run dev`
* Checkout the app:
    * In your browser go to: `localhost:8080`
    * Note: the app will automatically refresh when you change code
* Profit!

## License

Copyright (C) 2017 [Brian Douglass](http://bhdouglass.com/)

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License version 3, as published
by the Free Software Foundation.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranties of MERCHANTABILITY, SATISFACTORY QUALITY, or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program.  If not, see <http://www.gnu.org/licenses/>.
